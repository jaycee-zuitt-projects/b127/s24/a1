/*S24 Activity*/

/*2. Find users with letter s in their first name or d in their last name.
- Use the $or operator.
- Show only the firstName and lastName fields and hide the _id field.
Answer:
db.users.find({ $or: [{ firstName: { $regex: "S"} }, { lastName: { $regex: "D" }}]}, { _id: 0, firstName: 1, lastName: 1}).pretty();*/

/*3. Find users who are from the HR department and their age is greater then or equal to 70.
- Use the $and operator
Answer:
db.users.find({ $and: [{ age: { $gte: 70 } }, { department: "HR" } ] }).pretty();*/

/*4. Find users with the letter e in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators
Answer:
db.users.find({ $and: [{ firstName: { $regex: "e"} }, { age: { $lte: 30 }}]}).pretty();*/